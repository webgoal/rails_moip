require 'rails_helper'

module RailsMoip
  RSpec.describe MoipCallbackController, type: :controller do
    routes { RailsMoip::Engine.routes }

    context 'without Authorization header' do
      it 'has empty response' do
        post :create, params: { }
        expect(response).to be_unauthorized
      end
    end

    context 'with Authorization header' do
      before(:each) do
        request.headers['Authorization'] = ENV.fetch('MOIP_WEBHOOK_TOKEN')
      end

      context 'create with empty params' do
        it 'is unprocessable' do
          post :create, params: { }
          expect(response).to be_unprocessable
        end
      end

      context 'create with data' do
        let(:params) { { 'event': 'ORDER.PAID', 'resource': { 'x': 'y' } } }

        it 'has empty response' do
          post :create, params: params
          expect(response).to be_no_content
        end

        it 'generates event for lifecycle listener' do
          lifecycle_listener = spy
          Wisper.subscribe(lifecycle_listener)
          post :create, params: params
          expect(lifecycle_listener).to have_received(RailsMoip::EventPublisher::ORDER_UPDATED).with(params)
        end
      end
    end
  end
end
