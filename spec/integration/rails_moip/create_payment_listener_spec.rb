require 'rails_helper'

RSpec.describe RailsMoip::CreatePaymentListener do
  before(:each) { Wisper.clear }

  context 'with a valid payment_data' do
    let!(:payment_data) { { some: :thing } }
    let!(:order_gateway_id) { 'aaa' }
    let!(:api_class) { RailsMoip.gateway_api = spy }

    context 'with the listener subscribed' do
      before(:each) { Wisper.subscribe(subject) }

      it 'calls the gateway api' do
        RailsMoip::EventPublisher.new.publish(RailsMoip::EventPublisher::CREATE_PAYMENT, order_gateway_id, payment_data)
        expect(api_class).to have_received(:create_payment).with(order_gateway_id, payment_data)
      end

      context 'with an account lifecycle listener' do
        let!(:lifecycle_listener) { spy }
        before(:each) { Wisper.subscribe(lifecycle_listener) }

        it 'notify creation success' do
          allow(api_class).to receive(:create_payment).and_return(:data_return)
          RailsMoip::EventPublisher.new.publish(RailsMoip::EventPublisher::CREATE_PAYMENT, order_gateway_id, payment_data)
          expect(lifecycle_listener).to have_received(RailsMoip::EventPublisher::PAYMENT_CREATED).with(:data_return)
        end

        it 'notify creation fail' do
          allow(api_class).to receive(:create_payment).and_raise(RailsMoip::BusinessException)
          RailsMoip::EventPublisher.new.publish(RailsMoip::EventPublisher::CREATE_PAYMENT, order_gateway_id, payment_data)
          expect(lifecycle_listener).to have_received(RailsMoip::EventPublisher::PAYMENT_CREATION_ERROR)
        end
      end
    end
  end
end
