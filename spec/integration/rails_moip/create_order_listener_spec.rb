require 'rails_helper'

RSpec.describe RailsMoip::CreateOrderListener do
  before(:each) { Wisper.clear }

  context 'with a valid order_data' do
    let!(:order_data) { { some: :thing } }
    let!(:api_class) { RailsMoip.gateway_api = spy }

    context 'with the listener subscribed' do
      before(:each) { Wisper.subscribe(subject) }

      it 'calls the gateway api' do
        RailsMoip::EventPublisher.new.publish(RailsMoip::EventPublisher::CREATE_ORDER, order_data)
        expect(api_class).to have_received(:create_order).with(order_data)
      end

      context 'with an account lifecycle listener' do
        let!(:lifecycle_listener) { spy }
        before(:each) { Wisper.subscribe(lifecycle_listener) }

        it 'notify creation success' do
          allow(api_class).to receive(:create_order).and_return(:data_return)
          RailsMoip::EventPublisher.new.publish(RailsMoip::EventPublisher::CREATE_ORDER, order_data)
          expect(lifecycle_listener).to have_received(RailsMoip::EventPublisher::ORDER_CREATED).with(:data_return)
        end

        it 'notify creation fail' do
          allow(api_class).to receive(:create_order).and_raise(RailsMoip::BusinessException)
          RailsMoip::EventPublisher.new.publish(RailsMoip::EventPublisher::CREATE_ORDER, order_data)
          expect(lifecycle_listener).to have_received(RailsMoip::EventPublisher::ORDER_CREATION_ERROR)
        end
      end
    end
  end
end
