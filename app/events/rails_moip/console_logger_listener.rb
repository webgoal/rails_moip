module RailsMoip
  class ConsoleLoggerListener
    def log(*data)
      Rails.logger.info '--- ConsoleLoggerListener.log --- '
      Rails.logger.info data
      Rails.logger.info '--- ---'
    end
  end
end
