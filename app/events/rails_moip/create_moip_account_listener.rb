module RailsMoip
  class CreateMoipAccountListener
    def create_moip_account_event(data)
      return_data = RailsMoip.gateway_api.create_moip_account(data)
      RailsMoip::EventPublisher.new.publish(RailsMoip::EventPublisher::ACCOUNT_CREATED, return_data)
    rescue BusinessException => ex
      RailsMoip::EventPublisher.new.publish(RailsMoip::EventPublisher::ACCOUNT_CREATION_ERROR, { ex: ex, data: data })
    end
  end
end
