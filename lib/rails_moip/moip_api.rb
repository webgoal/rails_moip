module RailsMoip
  class MoipApi
    def initialize
      @moip_auth ||= Moip2::Auth::OAuth.new(ENV.fetch('MOIP_OAUTH_TOKEN'))
      @moip_client ||= Moip2::Client.new(ENV.fetch('MOIP_ENVIRONMENT'), @moip_auth)
      @moip_api ||= Moip2::Api.new(@moip_client)
    end

    def create_moip_account(data)
      process_response @moip_api.accounts.create(data)
    end

    def create_order(data)
      process_response @moip_api.order.create(data)
    end

    def create_payment(order_id, data)
      process_response @moip_api.payment.create(order_id, data)
    end

    private

    def process_response(response)
      raise BusinessException, response.errors if response.respond_to?(:errors)
      raise BusinessException, response unless response.success?
      response
    end
  end
end
