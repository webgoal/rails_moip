module RailsMoip
  class EventPublisher
    include Wisper::Publisher

    CREATE_MOIP_ACCOUNT = :create_moip_account_event
    ACCOUNT_CREATED = :account_created_event
    ACCOUNT_CREATION_ERROR = :account_creation_error_event

    CREATE_ORDER = :create_order_event
    ORDER_CREATED = :order_created_event
    ORDER_CREATION_ERROR = :order_creation_error_event
    ORDER_UPDATED = :order_updated_event

    CREATE_PAYMENT = :create_payment_event
    PAYMENT_CREATED = :payment_created_event
    PAYMENT_CREATION_ERROR = :payment_creation_error_event

    def publish(event, *args)
      broadcast(event, *args)
    end
  end
end
