$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "rails_moip/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "rails_moip"
  s.version     = RailsMoip::VERSION
  s.authors     = ["Atelie de Software Webgoal"]
  s.email       = ["contato@atelie.software"]
  s.homepage    = "http://atelie.software"
  s.summary     = "Rails to Moip integration."
  s.description = "Rails engine to easily connect to Moip Gateway."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.1.3"

  s.add_development_dependency "sqlite3"
end
