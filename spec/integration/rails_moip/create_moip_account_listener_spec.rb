require 'rails_helper'

RSpec.describe RailsMoip::CreateMoipAccountListener do
  before(:each) { Wisper.clear }

  context 'with a valid account_data' do
    let!(:account_data) { { some: :thing } }
    let!(:api_class) { RailsMoip.gateway_api = spy }

    context 'with the listener subscribed' do
      before(:each) { Wisper.subscribe(subject) }

      it 'calls the gateway api' do
        RailsMoip::EventPublisher.new.publish(RailsMoip::EventPublisher::CREATE_MOIP_ACCOUNT, account_data)
        expect(api_class).to have_received(:create_moip_account).with(account_data)
      end

      context 'with an account lifecycle listener' do
        let!(:lifecycle_listener) { spy }
        before(:each) { Wisper.subscribe(lifecycle_listener) }

        it 'notify creation success' do
          allow(api_class).to receive(:create_moip_account).and_return(:moip_return)
          RailsMoip::EventPublisher.new.publish(RailsMoip::EventPublisher::CREATE_MOIP_ACCOUNT, account_data)
          expect(lifecycle_listener).to have_received(RailsMoip::EventPublisher::ACCOUNT_CREATED).with(:moip_return)
        end

        it 'notify creation fail' do
          allow(api_class).to receive(:create_moip_account).and_raise(RailsMoip::BusinessException)
          RailsMoip::EventPublisher.new.publish(RailsMoip::EventPublisher::CREATE_MOIP_ACCOUNT, account_data)
          expect(lifecycle_listener).to have_received(RailsMoip::EventPublisher::ACCOUNT_CREATION_ERROR)
        end
      end
    end
  end
end
