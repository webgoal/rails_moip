require 'rails_helper'

RSpec.describe RailsMoip::MoipApi do
  describe '.new' do
    it 'creates a Moip2::Auth::OAuth' do
      expect(Moip2::Auth::OAuth).to receive(:new)
      RailsMoip::MoipApi.new
    end

    it 'creates a Moip2::Client' do
      expect(Moip2::Client).to receive(:new)
      RailsMoip::MoipApi.new
    end

    it 'creates a Moip2::Api' do
      expect(Moip2::Api).to receive(:new)
      RailsMoip::MoipApi.new
    end
  end

  describe 'api calls' do
    let!(:data) { { x: :y } }
    let!(:double_api) { double }
    let!(:double_response) { double }

    before(:each) {
      allow(Moip2::Api).to receive(:new).and_return(double_api)
    }

    describe '.create_moip_account' do
      let!(:double_accounts) { double }

      before(:each) {
        allow(double_api).to receive(:accounts).and_return(double_accounts)
      }

      it 'creates successfully' do
        allow(double_response).to receive(:success?).and_return(true)
        expect(double_accounts).to receive(:create).with(data).and_return(double_response)
        subject.create_moip_account(data)
      end

      it 'raise if there are validation problems' do
        allow(double_response).to receive(:success?).and_return(false)
        allow(double_accounts).to receive(:create).with(data).and_return(double_response)
        expect { subject.create_moip_account(data) }.to raise_error(RailsMoip::BusinessException)
      end

      it 'raise if there are errors on response' do
        allow(double_response).to receive(:errors).and_return([{}])
        allow(double_accounts).to receive(:create).with(data).and_return(double_response)
        expect { subject.create_moip_account(data) }.to raise_error(RailsMoip::BusinessException)
      end
    end

    context '.create_order' do
      let!(:double_order) { double }

      before(:each) {
        allow(double_api).to receive(:order).and_return(double_order)
      }

      it 'creates successfully' do
        allow(double_response).to receive(:success?).and_return(true)
        expect(double_order).to receive(:create).with(data).and_return(double_response)
        subject.create_order(data)
      end

      it 'raise if there are validation problems' do
        allow(double_response).to receive(:success?).and_return(false)
        allow(double_order).to receive(:create).with(data).and_return(double_response)
        expect { subject.create_order(data) }.to raise_error(RailsMoip::BusinessException)
      end

      it 'raise if there are errors on response' do
        allow(double_response).to receive(:errors).and_return([{}])
        allow(double_order).to receive(:create).with(data).and_return(double_response)
        expect { subject.create_order(data) }.to raise_error(RailsMoip::BusinessException)
      end
    end

    context '.create_payment' do
      let!(:double_payment) { double }
      let!(:gateway_id) { 'aaa' }

      before(:each) {
        allow(double_api).to receive(:payment).and_return(double_payment)
      }

      it 'creates successfully' do
        allow(double_response).to receive(:success?).and_return(true)
        expect(double_payment).to receive(:create).with(gateway_id, data).and_return(double_response)
        subject.create_payment(gateway_id, data)
      end

      it 'raise if there are validation problems' do
        allow(double_response).to receive(:success?).and_return(false)
        allow(double_payment).to receive(:create).with(gateway_id, data).and_return(double_response)
        expect { subject.create_payment(gateway_id, data) }.to raise_error(RailsMoip::BusinessException)
      end

      it 'raise if there are errors on response' do
        allow(double_response).to receive(:errors).and_return([{}])
        allow(double_payment).to receive(:create).with(gateway_id, data).and_return(double_response)
        expect { subject.create_payment(gateway_id, data) }.to raise_error(RailsMoip::BusinessException)
      end
    end
  end
end
