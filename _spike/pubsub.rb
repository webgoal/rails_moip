require 'wisper'

class CreateOrder # app 1
  include Wisper::Publisher

  def call()
    p 'CreateOrder.call()'
    data = { name: 'Altz', value: 10.98 }
    broadcast(:create_order, data)
  end
end

class CreateOrderListener # gem 2
  def self.create_order(data)
    p 'CreateOrderListener.create_order()'
    p data
  end
end

class OrderCreated # gem 3
  include Wisper::Publisher

  def call()
    p 'OrderCreated.call()'
    data = { email: 'beto@gato.com' }
    broadcast(:order_created, data)
  end
end

class OrderCreationListener # app 4
  def self.order_created(data)
    p 'OrderCreatedListener.order_created()'
    p data
  end

  def order_canceled(data)
    p 'OrderCreatedListener.order_canceled()'
    p data
  end

  def order_updated(data)
    p 'OrderCreatedListener.order_updated()'
    p data
  end
end



p 'Initializing...'

Wisper.subscribe(CreateOrderListener)
Wisper.subscribe(CreateOrderListener)

co = CreateOrder.new
co.call
