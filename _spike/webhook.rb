require 'moip2'

meu_app = {
  "id": "APP-LHAP9G3QP4JT",
  "website": "www.moipspike.com.br",
  "accessToken": "b2fd1c37e5374b48915df93fec5b89ea_v2",
  "description": "App de Spike",
  "name": "AppSpike",
  "secret": "08456042aaa9415ab99f3da9fdb7e684",
  "redirectUri": "http://www.moipspike.com.br/token",
  "createdAt": "2017-08-21T20:36:36.858Z",
  "updatedAt": "2017-08-21T20:36:36.858Z"
}

minha_conta_id = 'MPA-FEE19A7EB7BE'
meu_access_token = 'd6984c73b68e4800b432b58b43b54d81_v2'

webhook_creation_data = {
  "events": [
    "ORDER.*",
    "PAYMENT.*"
  ],
  "target": "https://requestb.in/18nc2cb1",
  "media": "WEBHOOK"
}

# Atenção! Authorization não funciona com OAuth, só basic!

# para criar, execute:
# curl -H "Content-Type: application/json" -H 'Authorization: Basic V1dKTEZMQkxSMllWWTdYQ0tXWk9JUVowVVBRUlFHRE06VkxPVjFYSFNXTlBDRUFQMEFRT1hFQlA1QkxSUkZLTUdUTUZCSjNSQw=='  -d '{"events":["ORDER.*","PAYMENT.*"],"target":"https://0bbbdb49.ngrok.io/payment_gateway/moip_callback","media":"WEBHOOK"}' --request POST --url https://sandbox.moip.com.br/v2/preferences/APP-LHAP9G3QP4JT/notifications

# para consultar, execute:
# curl -H 'Authorization: Basic V1dKTEZMQkxSMllWWTdYQ0tXWk9JUVowVVBRUlFHRE06VkxPVjFYSFNXTlBDRUFQMEFRT1hFQlA1QkxSUkZLTUdUTUZCSjNSQw==' --request GET --url https://sandbox.moip.com.br/v2/preferences/APP-LHAP9G3QP4JT/notifications

# para remover, execute:
# curl -H 'Authorization: Basic V1dKTEZMQkxSMllWWTdYQ0tXWk9JUVowVVBRUlFHRE06VkxPVjFYSFNXTlBDRUFQMEFRT1hFQlA1QkxSUkZLTUdUTUZCSjNSQw==' --request DELETE --url https://sandbox.moip.com.br/v2/preferences/notifications/NPR-ACNOO1P0KFKC
