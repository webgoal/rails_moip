module RailsMoip
  class LifecycleLoggerListener
    def account_created_event(event_name)
      Rails.logger.info'### :account_created_event ###'
    end

    def account_creation_error_event(event_name)
      Rails.logger.info'### :account_creation_error_event ###'
    end

    def order_created_event(event_name)
      Rails.logger.info'### :order_created_event ###'
    end

    def order_creation_error_event(event_name)
      Rails.logger.info'### :order_creation_error_event ###'
    end

    def order_updated_event(event_name)
      Rails.logger.info'### :order_updated_event ###'
    end

    def payment_created_event(event_name)
      Rails.logger.info'### :payment_created_event ###'
    end

    def payment_creation_error_event(event_name)
      Rails.logger.info'### :payment_creation_error_event ###'
    end

    def payment_updated_event(event_name)
      Rails.logger.info'### :payment_updated_event ###'
    end
  end
end
