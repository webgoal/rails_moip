RailsMoip::Engine.routes.draw do
  resources :moip_callback, only: [:create]
end
