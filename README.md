# RailsMoip

Gem para facilitar o uso do Gateway de Pagamentos Moip no Rails.

## Configuração inicial

Veja spike/completo.rb para curls de criação do App / Credenciais OAuth.

Configure as variáveis de ambiente (exemplo para docker-compose.yml):

```yml
MOIP_ENVIRONMENT: 'sandbox'
MOIP_OAUTH_TOKEN: 'd6984c73b68e4800b432b58b43b54d81_v2'
MOIP_ACCOUNT: 'MPA-FEE19A7EB7BE'
```

Configure um initializer com seus listeners. Veja spec/test_app/config/initializers/wisper.rb

Configure a rota para a Gem. Veja spec/test_app/config/routes.rb

Veja spike/webhooks.rb para curls de criação do Webhook.


## Exemplo de uso no Rails Console

Execute no terminal esta configuração antes de usar qualquer dos exemplos abaixo:

```ruby
# Criar listeners para acompanhar os logs no terminal
Wisper.subscribe(RailsMoip::ConsoleLoggerListener.new, with: :log) # loga todos os eventos
Wisper.subscribe(RailsMoip::LifecycleLoggerListener.new) # informa se deu certo ou errado a criacao da conta

# Criar listeners para efetiva criação dos itens
Wisper.subscribe(RailsMoip::CreateMoipAccountListener.new)
Wisper.subscribe(RailsMoip::CreateOrderListener.new)
Wisper.subscribe(RailsMoip::CreatePaymentListener.new)
```

Para criar uma Conta Moip:

```ruby
# Dados de exemplo
def moip_account_data
  {
    email: {
      address: FFaker::Internet.email
    },
    company: {
      name: "Webgoal",
      businessName: "Webgoal Aplicacoes e Solucoes Web LTDA",
      openingDate: "2008-04-01",
      taxDocument: {
        type: "CNPJ",
        number: FFaker::IdentificationBR.pretty_cnpj
      },
      address: {
        street: "Av. Joao Pinheiro",
        streetNumber: "341",
        district: "Campo da Mogiana",
        zipCode: "37701-880",
        city: "Pocos de Caldas",
        state: "MG",
        country: "BRA"
      },
      phone: {
        countryCode: "55",
        areaCode: "35",
        number: "37223776"
      },
    },
    person: {
      name: FFaker::Name.name,
      birthDate: "1981-12-25",
      taxDocument: {
        type: "CPF",
        number: FFaker::IdentificationBR.pretty_cpf
      },
      address: {
        street: "Rua Marcia Miliani",
        streetNumber: "795",
        district: "Alto da Boa Vista",
        zipCode: "37701-000",
        city: "Pocos de Caldas",
        state: "MG",
        country: "BRA"
      },
      phone: {
        countryCode: "55",
        areaCode: "35",
        number: "991774029"
      },
    },
    type: "MERCHANT"
  }
end

# Envio do evento:
RailsMoip::EventPublisher.new.publish(RailsMoip::EventPublisher::CREATE_MOIP_ACCOUNT, moip_account_data)
```

Para criar um Pagamento:

```ruby
# Dados de exemplo
def order_data
  minha_conta_id = 'MPA-FEE19A7EB7BE'
  conta_criada_id = 'MPA-1B1155294C04'

  {
    own_id: "my_generated_id",
    items: [
      {
        product: "Nome do produto",
        quantity: 2,
        detail: "Mais info...",
        price: 90000
      }
    ],
    customer: {
      own_id: "my_customer_1",
      fullname: "Cliente Tal",
      email: "cliente@email.com",
    },
    receivers: [
      {
        type: 'PRIMARY',
        moipAccount: { id: minha_conta_id },
        amount: {
          percentual: 70 # ou fixed: 1234, para especificar um valor fixo
        }
      },
      {
        type: 'SECONDARY',
        moipAccount: { id: conta_criada_id },
        amount: {
          percentual: 30 # ou fixed: 1234, para especificar um valor fixo
        }
      },
    ]
  }
end

# Envio do evento:
RailsMoip::EventPublisher.new.publish(RailsMoip::EventPublisher::CREATE_ORDER, order_data)
```
