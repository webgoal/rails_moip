require "rails_moip/engine"

module RailsMoip
  autoload :EventPublisher, 'rails_moip/event_publisher'
  autoload :MoipApi, 'rails_moip/moip_api'

  mattr_writer :gateway_api
  def self.gateway_api
    return @@gateway_api || MoipApi.new
  end
end
