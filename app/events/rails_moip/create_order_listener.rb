module RailsMoip
  class CreateOrderListener
    def create_order_event(data)
      return_data = RailsMoip.gateway_api.create_order(data)
      RailsMoip::EventPublisher.new.publish(RailsMoip::EventPublisher::ORDER_CREATED, return_data)
    rescue BusinessException => ex
      RailsMoip::EventPublisher.new.publish(RailsMoip::EventPublisher::ORDER_CREATION_ERROR, ex)
    end
  end
end
