require 'moip2'

# Passos para criar o App no Moip

# 1 - criar o app (https://sandbox.moip.com.br/v2/channels)
# USAR: curl -v -X POST -H "Content-Type: application/x-www-form-urlencoded" -H "Authorization: Basic WjFBWENJVkxGRjFDNUxYS1E1NldTOFdZRk1MVjFBVzk6VktWNkxJU0lRR0xSOU9KR1pMSVhIQ05VNzZDWlVSSks3T0JQVFc4Ug==" -H "Cache-Control: no-cache" -d "description=Plataforma+DSOP+de+gerenciamento+dos+servi%C3%A7os&name=DSOP+Sistema&redirectUri=https%3A%2F%2Fsistema.dsop.com.br&site=https%3A%2F%2Fsistema.dsop.com.br" "https://sandbox.moip.com.br/v2/channels"
# DSOP Metadata via https://documentao-moip.readme.io/v2.0/reference#criar-um-app

meu_app = {
  "id": "APP-LHAP9G3QP4JT",
  "website": "www.moipspike.com.br",
  "accessToken": "b2fd1c37e5374b48915df93fec5b89ea_v2",
  "description": "App de Spike",
  "name": "AppSpike",
  "secret": "08456042aaa9415ab99f3da9fdb7e684",
  "redirectUri": "http://www.moipspike.com.br/token",
  "createdAt": "2017-08-21T20:36:36.858Z",
  "updatedAt": "2017-08-21T20:36:36.858Z"
}

# 2 - autorizar o app na sua conta
# USAR: https://connect-sandbox.moip.com.br/oauth/authorize?response_type=code&client_id=[ID-DO-SEU-APP!!!]&redirect_uri=[URL-REDIRECIONAMENTO-USADA-AO-CRIAR-APP]&scope=RECEIVE_FUNDS,REFUND,MANAGE_ACCOUNT_INFO,RETRIEVE_FINANCIAL_INFO,TRANSFER_FUNDS
# EXEMPLO: https://connect-sandbox.moip.com.br/oauth/authorize?response_type=code&client_id=APP-LHAP9G3QP4JT&redirect_uri=http://www.moipspike.com.br/token&scope=RECEIVE_FUNDS,REFUND,MANAGE_ACCOUNT_INFO,RETRIEVE_FINANCIAL_INFO,TRANSFER_FUNDS
#
# RETORNO: http://www.moipspike.com.br/token?code=7b49a3ea9f5ca33145cff4d2dc8f74d8d9dca03f
# Usar este code que veio na url, tem duração de 10 anos, recomendado usar a outra api para renovar pelo menos uma vez por mês.
meu_code = '7b49a3ea9f5ca33145cff4d2dc8f74d8d9dca03f'


# 3 - gerar o token para uso nas chamadas
# USAR: curl -X POST -H "Content-Type: application/x-www-form-urlencoded" -H "Authorization: Basic [SEU TOKEN BASIC]" -H "Cache-Control: no-cache" -d 'client_id=[ID-DO-SEU-APP]&client_secret=[CLIENT-SECRET-DO-APP]&grant_type=authorization_code&code=[SEU-CODE]&redirect_uri=[URL-REDIRECIONAMENTO-USADA-AO-CRIAR-APP]' "https://connect-sandbox.moip.com.br/oauth/token"
# # para gerar o header de autorizacao basic:
# require "base64"
# @token = ENV.fetch('MOIP_TOKEN')
# @secret = ENV.fetch('MOIP_SECRET')
# %(Basic #{Base64.strict_encode64("#{@token}:#{@secret}")})
# Resultado: "Basic V1dKTEZMQkxSMllWWTdYQ0tXWk9JUVowVVBRUlFHRE06VkxPVjFYSFNXTlBDRUFQMEFRT1hFQlA1QkxSUkZLTUdUTUZCSjNSQw=="

# EXEMPLO: curl -X POST -H "Content-Type: application/x-www-form-urlencoded" -H "Authorization: Basic V1dKTEZMQkxSMllWWTdYQ0tXWk9JUVowVVBRUlFHRE06VkxPVjFYSFNXTlBDRUFQMEFRT1hFQlA1QkxSUkZLTUdUTUZCSjNSQw==" -H "Cache-Control: no-cache" -d 'client_id=APP-LHAP9G3QP4JT&client_secret=08456042aaa9415ab99f3da9fdb7e684&grant_type=authorization_code&code=7b49a3ea9f5ca33145cff4d2dc8f74d8d9dca03f&redirect_uri=http://www.moipspike.com.br/token' "https://connect-sandbox.moip.com.br/oauth/token"
# Retorno: {"accessToken":"d6984c73b68e4800b432b58b43b54d81_v2","access_token":"d6984c73b68e4800b432b58b43b54d81_v2","expires_in":"2027-08-24","refreshToken":"74549461a91c4d70b71b19e1a4959fdc_v2","refresh_token":"74549461a91c4d70b71b19e1a4959fdc_v2","scope":"RECEIVE_FUNDS,REFUND,MANAGE_ACCOUNT_INFO,RETRIEVE_FINANCIAL_INFO,TRANSFER_FUNDS","moipAccount":{"id":"MPA-FEE19A7EB7BE"}}
minha_conta_id = 'MPA-FEE19A7EB7BE'
meu_access_token = 'd6984c73b68e4800b432b58b43b54d81_v2'

# 4 - refresh (não testado!)



auth = Moip2::Auth::OAuth.new(meu_access_token)

client = Moip2::Client.new(ENV.fetch('MOIP_ENVIRONMENT'), auth)
api = Moip2::Api.new(client)

data = {
  email: {
    address: "tots2@teste.com.br",
  },
  person: {
    name: "Joaquim José",
    birthDate: "1990-01-01",
    taxDocument: {
      type: "CPF",
      number: "468.839.286-93",
    },
    address: {
      street: "Av. Brigadeiro Faria Lima",
      streetNumber: "2927",
      district: "Itaim",
      zipCode: "01234-000",
      city: "S\u00E3o Paulo",
      state: "SP",
      country: "BRA",
    },
    phone: {
      countryCode: "55",
      areaCode: "11",
      number: "965213244",
    },
  },
  type: "MERCHANT",
}

account = api.accounts.create(data)
puts account

# #<RecursiveOpenStruct id="MPA-1B1155294C04", login="tots@teste.com.br", access_token="6ec372b476a84b9487055804a05a3f65_v2", channel_id="APP-LHAP9G3QP4JT", type="MERCHANT", transparent_account=false, email={:address=>"tots@teste.com.br", :confirmed=>false}, person={:name=>"Joaquim", :last_name=>"José", :birth_date=>"1990-01-01", :tax_document=>{:type=>"CPF", :number=>"161.113.846-94"}, :address=>{:street=>"Av. Brigadeiro Faria Lima", :street_number=>"2927", :district=>"Itaim", :zipcode=>"01234000", :zip_code=>"01234000", :city=>"São Paulo", :state=>"SP", :country=>"BRA"}, :phone=>{:country_code=>"55", :area_code=>"11", :number=>"965213244"}}, created_at="2017-08-24T13:25:43.474Z", _links={:self=>{:href=>"https://sandbox.moip.com.br/moipaccounts/MPA-1B1155294C04", :title=>nil}, :set_password=>{:href=>"https://desenvolvedor.moip.com.br/sandbox/AskForNewPassword.do?method=confirm&email=tots%40teste.com.br&code=e85a109c7808df38a403e22fbf9a51fd"}}>

# conta_criada_id = 'MPA-1B1155294C04'


# order = api.order.create({
#   own_id: "ruby_sdk_1",
#   items: [
#     {
#       product: "Nome do produto",
#       quantity: 2,
#       detail: "Mais info...",
#       price: 90000
#     }
#   ],
#   customer: {
#     own_id: "ruby_sdk_customer_1",
#     fullname: "Jose da Silva",
#     email: "sandbox_v2_1401147277@email.com",
#   },
#   receivers: [
#     {
#       type: 'PRIMARY',
#       feePayor: true,
#       moipAccount: { id: minha_conta_id },
#       amount: {
#         percentual: 70 # ou fixed: 1234, para especificar um valor fixo
#       }
#     },
#     {
#       type: 'SECONDARY',
#       feePayor: false,
#       moipAccount: { id: conta_criada_id },
#       amount: {
#         percentual: 30 # ou fixed: 1234, para especificar um valor fixo
#       }
#     },
#   ]
# })
#
# p order.inspect

# order_criada_id = "ORD-2H4GHLO8QHU9"
#
# payment = api.payment.create(order_criada_id,
#     {
#         funding_instrument: {
#             method: "BOLETO",
#             boleto: {
#                 expiration_date: "2017-09-30",
#                 instruction_lines: {
#                   first: "Primeira linha do boleto",
#                   second: "Segunda linha do boleto",
#                   third: "Terceira linha do boleto"
#                 }
#             }
#         }
#     }
# )
#
# p payment.inspect
