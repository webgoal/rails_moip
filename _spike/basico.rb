require 'moip2'

auth = Moip2::Auth::Basic.new(ENV.fetch('MOIP_TOKEN'), ENV.fetch('MOIP_SECRET'))

client = Moip2::Client.new(ENV.fetch('MOIP_ENVIRONMENT'), auth)
api = Moip2::Api.new(client)

order = api.order.create({
  own_id: "ruby_sdk_1",
  items: [
    {
      product: "Nome do produto",
      quantity: 2,
      detail: "Mais info...",
      price: 90000
    }
  ],
  customer: {
    own_id: "ruby_sdk_customer_1",
    fullname: "Jose da Silva",
    email: "sandbox_v2_1401147277@email.com",
  }
})

p order.inspect

payment = api.payment.create(order.id,
    {
        funding_instrument: {
            method: "BOLETO",
            boleto: {
                expiration_date: "2017-09-30",
                instruction_lines: {
                  first: "Primeira linha do boleto",
                  second: "Segunda linha do boleto",
                  third: "Terceira linha do boleto"
                }
            }
        }
    }
)

p payment.inspect
