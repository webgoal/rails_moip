module RailsMoip
  class CreatePaymentListener
    def create_payment_event(order_id, data)
      return_data = RailsMoip.gateway_api.create_payment(order_id, data)
      RailsMoip::EventPublisher.new.publish(RailsMoip::EventPublisher::PAYMENT_CREATED, return_data)
    rescue BusinessException => ex
      RailsMoip::EventPublisher.new.publish(RailsMoip::EventPublisher::PAYMENT_CREATION_ERROR, ex)
    end
  end
end
