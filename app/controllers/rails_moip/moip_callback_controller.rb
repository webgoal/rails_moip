require_dependency "rails_moip/application_controller"

module RailsMoip
  class MoipCallbackController < ApplicationController
    skip_before_action :verify_authenticity_token, only: :create
    before_action :check_authorization, only: [:create]

    def create
      RailsMoip::EventPublisher.new.publish(RailsMoip::EventPublisher::ORDER_UPDATED, {
        event: event_param,
        resource: resource_param
      })
    rescue ActionController::ParameterMissing => ex
      head :unprocessable_entity
    end

    private
    def event_param
      params.require(:event)
    end

    def resource_param
      params.require(:resource).permit!.to_h
    end

    def check_authorization
      head :unauthorized unless request.headers['Authorization'] == ENV.fetch('MOIP_WEBHOOK_TOKEN')
    end
  end
end
